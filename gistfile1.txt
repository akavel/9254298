From: Florian Nücke <florian <at> nuecke.de>
Subject: Re: "Image-based persistence" a la Smalltalk/LISP?
Newsgroups: gmane.comp.lang.lua.general
Date: 2014-02-27 15:10:41 GMT (5 days, 20 hours and 9 minutes ago)

If I understand the requirement correctly, with some limitations it is
possible to do this with Pluto/Eris using a simple "init" script that
gets fed to the interpreter. Since I had a portion of this lying around
from earlier tests and found the idea interesting, I put a naive
approach for such a script together [1]. It works well for simple things
(i.e. anything not userdata).

The limitations (I could think of, there are bound to be more) I
outlined in the comment on top of the script. The main issue is
userdata, of course, which cannot be persisted automatically - so even
if you "only" have open file handles flying around, it will fail. That's
an issue with any approach on persistence, though.

Florian

[1] http://pastebin.com/Xv8NN85y